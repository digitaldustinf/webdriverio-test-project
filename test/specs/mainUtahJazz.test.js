import UtahJazzPage from  '../pageobjects/utahjazz.page';
import ScholarshipPage from  '../pageobjects/scholarship.page';
import RosterPage from '../pageobjects/roster.page';

var assert = require('assert') 

describe('Main Utah Jazz page',  () => {
    it("Verify hero slide", async () => {
        await UtahJazzPage.open()
        await (await UtahJazzPage.heroThankYouSlide).waitForDisplayed({ timeout: 5000})
    })

    it("Check scholarship tracker section", async () =>  {
        await UtahJazzPage.open()

        // Scroll to scholarship tracker section
        await UtahJazzPage.scrollToScholarshipSection();
        await (await UtahJazzPage.scholarshipSection).isDisplayedInViewport()

        // Check scholarship tracker title text
        let scholarshipText = await (await UtahJazzPage.scholarshipTitleElement).getText()
        assert.strictEqual(scholarshipText, 'Scholarship Tracker')

        // Press Learn more button
        await UtahJazzPage.clickLearnMoreButton()
        await (await ScholarshipPage.utahJazzIntro).waitForDisplayed({ timeout: 8000})
        assert.strictEqual(await browser.getUrl(), "https://www.nba.com/jazz/scholarship")
    })

    it("Check and use dropdown menu", async () => {
        await UtahJazzPage.open()

        // Check & hover on team tab
        let teamText = await (await UtahJazzPage.teamTab).getText()
        assert.strictEqual(teamText, 'TEAM')
        await UtahJazzPage.hoverOnTeamTab()
        
        // Verify first item under team tab
        let rosterText = await (await UtahJazzPage.rosterButtonTeamTab).getText()
        assert.strictEqual(rosterText, "ROSTER")
    
        // Click on first item, confirm taken to the correct page
        await UtahJazzPage.clickOnRosterButton() 
        await (await RosterPage.rosterImage).waitForDisplayed({ timeout: 8000})
        assert.strictEqual(await browser.getUrl(), "https://www.nba.com/jazz/roster")
    })
})