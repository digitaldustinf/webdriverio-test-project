import ScholarshipPage from  '../pageobjects/scholarship.page';
var assert = require('assert') 

describe('Jazz Scholarship page', () => {
    it('Check scholarship tracker number of scholarships', async () => {
        await ScholarshipPage.open()
        browser.pause(4000)
        await (await ScholarshipPage.utahJazzIntro).waitForDisplayed({ timeout: 8000})
        
        // Check scholarship number is correct
        assert.strictEqual(await (await ScholarshipPage.scholarshipCount).getText(), '61')
    }) 
    it('Check youtube video', async () => {
        await ScholarshipPage.open()
        await (await ScholarshipPage.utahJazzIntro).waitForDisplayed({ timeout: 3000})

        // Check video
        await ScholarshipPage.scrollToScholarshipVideo()
        assert.strictEqual(await (await ScholarshipPage.videoScholarshipWinners).isDisplayedInViewport(), true, "youtube video is not in viewport")
        assert.strictEqual(await (await ScholarshipPage.videoScholarshipWinners).isClickable(), true, "youtube video is not clickable")
    })
})