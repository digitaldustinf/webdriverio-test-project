import Page from './page';

class ScholarshipPage extends Page {
    
    get utahJazzIntro() { return $('#ujs_intro') }
    get scholarshipCount() { return $('#jazz-scholarship-tracker-scholarships-number') }

    get videoScholarshipWinners() { return $('.ujs-video')}

    async scrollToScholarshipVideo() {
        await (await this.videoScholarshipWinners).scrollIntoView()
    }
    get moviePlayer() { return $('#player') }
    
    open () {
        return super.open('jazz/scholarship');
    }
}

export default new ScholarshipPage();
