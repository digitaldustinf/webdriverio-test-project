import Page from './page';

class RosterPage extends Page {
   
    get rosterImage() { return $('.jazz-page-header')}
    
    open () {
        return super.open('jazz/scholarship');
    }
}

export default new RosterPage();
