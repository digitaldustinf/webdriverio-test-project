import Page from './page';

class UtahJazzPage extends Page {
    /**
     * define selectors using getter methods
     */
    get teamTab () { return $('#main-menu li:nth-child(5)') }
    get rosterButtonTeamTab() { return $(' #main-menu li:nth-child(5) ul li:nth-child(1) ul li.first.leaf.em-smi')} 

    async hoverOnTeamTab() {
        await (await this.teamTab).moveTo()
        await (await this.rosterButtonTeamTab).waitForDisplayed()
    }
    
    async clickOnRosterButton() {
        await (await this.rosterButtonTeamTab).click()
    }

    get heroThankYouSlide() { return $('#jazz_section-hero')} 

    get scholarshipSection() { return $('#jazz-scholarship-tracker') } 
    async scrollToScholarshipSection() {
        await (await this.scholarshipSection).scrollIntoView()
    }

    get scholarshipTitleElement() { return $('//*[@id="jazz-scholarship-tracker"]/div/h2') } 
    get scholarshipLearnMoreButton() { return $('//*[@id="jazz-scholarship-tracker"]/div/div/div[1]/p[3]/a') }
    
    async clickLearnMoreButton() {
        await (await this.scholarshipLearnMoreButton).click()
    }

    /**
     * overwrite specifc options to adapt it to page object
     */
    open () {
        return super.open('jazz');
    }
}

export default new UtahJazzPage();
